const dynamoose = require('dynamoose');
const Schema = dynamoose.Schema;
const shortid = require('shortid');

const CategorySchema = new Schema({
    id: {
        type: String,
        default: shortid.generate(),
        hashKey: true
    },
    tag: {
        type: String,
        required: true,
        index: {
            global: true,
            project: true
        }
    },
    name: {
        type: String,
        required: true
    },
    icon: {
        type: String,
        required: true
    }
});

CategorySchema.statics.getAll = async function(cb) {
    let results = await this.scan().exec();
    while (results.lastKey) {
        results = await this.scan()
            .startKey(results.startKey)
            .exec();
    }
    return results;
};

module.exports = Category = dynamoose.model('categories', CategorySchema, {
    update: process.env.ENVIRONMENT == 'development' ? true : false
});
