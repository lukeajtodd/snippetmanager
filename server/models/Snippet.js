const dynamoose = require('dynamoose');
const Schema = dynamoose.Schema;
const shortid = require('shortid');

const SnippetSchema = new Schema({
    id: {
        type: String,
        default: shortid.generate(),
        hashKey: true
    },
    userId: {
        type: String,
        required: true
    },
    categoryId: {
        type: String,
        required: true
    },
    description: {
        type: String,
        required: true
    },
    code: {
        type: String,
        required: true
    },
    title: {
        type: String,
        required: true
    },
    likes: {
        type: [Object],
        required: true,
        default: []
    },
    comments: {
        type: [Object],
        required: true,
        default: []
    },
    date: {
        type: Date,
        default: Date.now()
    }
});

SnippetSchema.statics.getAll = async function(cb) {
    let results = await this.scan().exec();
    while (results.lastKey) {
        results = await this.scan()
            .startKey(results.startKey)
            .exec();
    }
    return results;
};

module.exports = Snippet = dynamoose.model('snippet', SnippetSchema);
