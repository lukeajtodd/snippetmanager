const { GraphQLDate } = require('graphql-iso-date');

const { resolvers: Auth } = require('./auth');
const { resolvers: Categories } = require('./categories');
const { resolvers: Snippets } = require('./snippets');

const globals = {
    Date: GraphQLDate
};

module.exports = [globals, Auth, Categories, Snippets];
