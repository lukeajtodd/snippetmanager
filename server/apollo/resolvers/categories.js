const { ApolloError, AuthenticationError } = require('apollo-server');
const axios = require('axios');
const resolverUrl = `/api/categories/`;

const resolvers = {
    Query: {
        categories: (_, args, ctx) => {
            if (!ctx.auth) {
                throw new AuthenticationError('Must be logged in.');
            }
            return axios
                .get(`${ctx.baseUrl}${resolverUrl}`, {
                    headers: { Authorization: ctx.auth }
                })
                .then(({ data }) => data)
                .catch(err => {
                    throw new ApolloError(err.message, 'API_ERROR');
                });
        },
        category: (_, args, ctx) => {
            const { id } = args;
            if (!ctx.auth) {
                throw new AuthenticationError('Must be logged in.');
            }
            return axios
                .get(`${ctx.baseUrl}${resolverUrl}${id}`, {
                    headers: { Authorization: ctx.auth }
                })
                .then(({ data }) => data)
                .catch(err => {
                    throw new ApolloError(err.message, 'API_ERROR');
                });
        }
    }
};

module.exports = { resolvers };
