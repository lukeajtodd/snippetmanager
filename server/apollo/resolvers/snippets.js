const { ApolloError, AuthenticationError } = require('apollo-server');
const axios = require('axios');
const resolverUrl = `/api/snippets/`;

const resolvers = {
    Query: {
        snippets: (_, args, ctx) => {
            if (!ctx.auth) {
                throw new AuthenticationError('Must be logged in.');
            }
            return axios
                .get(`${ctx.baseUrl}${resolverUrl}`, {
                    headers: { Authorization: ctx.auth }
                })
                .then(({ data }) => data)
                .catch(err => {
                    throw new ApolloError(err.message, 'API_ERROR');
                });
        },
        snippet: (_, args, ctx) => {
            const { id } = args;
            if (!ctx.auth) {
                throw new AuthenticationError('Must be logged in.');
            }
            return axios
                .get(`${ctx.baseUrl}${resolverUrl}/${id}`, {
                    headers: { Authorization: ctx.auth }
                })
                .then(({ data }) => data)
                .catch(err => {
                    throw new ApolloError(err.message, 'API_ERROR');
                });
        }
    },
    Mutation: {
        addSnippet: (_, args, ctx) => {
            const { code, description, title, userId, tag } = args;
            if (!ctx.auth) {
                throw new AuthenticationError('Must be logged in.');
            }
            return axios
                .post(
                    `${ctx.baseUrl}${resolverUrl}`,
                    {
                        code,
                        description,
                        title,
                        userId,
                        tag
                    },
                    {
                        headers: { Authorization: ctx.auth }
                    }
                )
                .then(({ data }) => data)
                .catch(err => {
                    throw new ApolloError(err.message, 'API_ERROR');
                });
        },
        deleteSnippet: (_, args, ctx) => {
            const { id } = args;
            if (!ctx.auth) {
                throw new AuthenticationError('Must be logged in.');
            }
            return axios
                .delete(`${ctx.baseUrl}${resolverUrl}/${id}`, {
                    headers: { Authorization: ctx.auth }
                })
                .then(({ data }) => data)
                .catch(err => {
                    throw new ApolloError(err.message, 'API_ERROR');
                });
        },
        like: (_, args, ctx) => {
            const { id } = args;
            if (!ctx.auth) {
                throw new AuthenticationError('Must be logged in.');
            }
            return axios
                .post(`${ctx.baseUrl}${resolverUrl}/like/${id}`, {
                    headers: { Authorization: ctx.auth }
                })
                .then(({ data }) => data)
                .catch(err => {
                    throw new ApolloError(err.message, 'API_ERROR');
                });
        },
        unlike: (_, args, ctx) => {
            const { id } = args;
            if (!ctx.auth) {
                throw new AuthenticationError('Must be logged in.');
            }
            return axios
                .post(`${ctx.baseUrl}${resolverUrl}/unlike/${id}`, {
                    headers: { Authorization: ctx.auth }
                })
                .then(({ data }) => data)
                .catch(err => {
                    throw new ApolloError(err.message, 'API_ERROR');
                });
        },
        addComment: (_, args, ctx) => {
            const { id, text } = args;
            if (!ctx.auth) {
                throw new AuthenticationError('Must be logged in.');
            }
            return axios
                .post(
                    `${ctx.baseUrl}${resolverUrl}/comment/${id}`,
                    {
                        text
                    },
                    {
                        headers: { Authorization: ctx.auth }
                    }
                )
                .then(({ data }) => data)
                .catch(err => {
                    throw new ApolloError(err.message, 'API_ERROR');
                });
        },
        deleteComment: (_, args, ctx) => {
            const { id } = args;
            if (!ctx.auth) {
                throw new AuthenticationError('Must be logged in.');
            }
            return axios
                .delete(`${ctx.baseUrl}${resolverUrl}/comment/${id}`, {
                    headers: { Authorization: ctx.auth }
                })
                .then(({ data }) => data)
                .catch(err => {
                    throw new ApolloError(err.message, 'API_ERROR');
                });
        }
    }
};

module.exports = { resolvers };
