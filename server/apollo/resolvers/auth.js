const axios = require('axios');
const resolverUrl = `/api/auth/`;

const resolvers = {
    Mutation: {
        register: (_, args, ctx) => {
            let { name, email, password, password2 } = args;
            return axios
                .post(`${ctx.baseUrl}${resolverUrl}/register`, {
                    name,
                    email,
                    password,
                    password2
                })
                .then(({ data }) => data);
        },
        login: (_, args, ctx) => {
            let { email, password } = args;
            return axios
                .post(`${ctx.baseUrl}${resolverUrl}/login`, {
                    email,
                    password
                })
                .then(({ data }) => data);
        }
    }
};

module.exports = { resolvers };
