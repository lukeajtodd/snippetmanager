const { typeDefs: Auth } = require('./auth');
const { typeDefs: Categories } = require('./categories');
const { typeDefs: Snippets } = require('./snippets');

const globals = `
    type Query {
        greetings: String!
    }
    type Mutation {
        placeholder: String!
    }
`;

module.exports = [globals, Auth, Categories, Snippets];
