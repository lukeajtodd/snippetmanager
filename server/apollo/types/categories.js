const typeDefs = `
    type Category {
        id: String!
        tag: String!
        name: String!
        icon: String!
    }

    extend type Query {
        categories: [Category!]!
        category(id: Int!): Category!
    }
`;

module.exports = { typeDefs };
