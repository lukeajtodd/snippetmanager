const typeDefs = `

    type Like {
        userId: String!
    }

    type Comment {
        id: String!
        text: String!
        name: String!
        userId: String!
    }

    type Snippet {
        id: String!
        userId: String!
        categoryId: String!
        description: String!
        code: String!
        title: String!
        likes: [Like]
        comments: [Comment]
    }

    type DeleteResponse {
        success: Boolean!
    }

    extend type Query {
        snippets: [Snippet!]!
        snippet(id: String!): Snippet!   
    }

    extend type Mutation {
        addSnippet(code: String!, description: String!, title: String!, userId: String!, tag: String!): Snippet!
        deleteSnippet(id: String!): DeleteResponse!
        like(id: String!): Snippet!
        unlike(id: String!): Snippet!
        addComment(id: String!, text: String!): Snippet!
        deleteComment(id: String!): Snippet!
    }
`;

module.exports = { typeDefs };
