const express = require('express');
const router = express.Router();
const mongoose = require('mongoose');
const passport = require('passport');

const Category = require('@models/Category');
const Snippet = require('@models/Snippet');

// @route   GET api/categories
// @desc    Get all snippet categories
// @access  Private
router.get(
    '/',
    passport.authenticate('jwt', { session: false }),
    (req, res) => {
        const errors = {};

        Category.getAll()
            .then(categories => res.json(categories))
            .catch(err => {
                errors.nocategoryfound = 'No categories found';
                res.status(404).json(errors);
            });
    }
);

// @route   GET api/categories/:id
// @desc    Get category by id
// @access  Private
router.get(
    '/:id',
    passport.authenticate('jwt', { session: false }),
    (req, res) => {
        const errors = {};

        Category.get({ id: req.params.id })
            .then(category => res.json(category))
            .catch(err => {
                errors.nocategoryfound = 'No category with that ID found';
                res.status(404).json(errors);
            });
    }
);

// @route   GET api/categories/:id/snippets
// @desc    Get all snippets by category id
// @access  Private
router.get(
    '/:id/snippets',
    passport.authenticate('jwt', { session: false }),
    (req, res) => {
        const errors = {};

        Category.get({ id: req.params.id })
            .then(category => {
                Snippet.getAll()
                    .then(snippets => {
                        let snippetsFiltered = snippets.filter(snippet => {
                            return snippet.categoryId === req.params.id;
                        });
                        if (snippetsFiltered.length) {
                            res.json(snippetsFiltered);
                        } else {
                            errors.nosnippetfound =
                                'No snippets under that category found';
                            res.status(404).json(errors);
                        }
                    })
                    .catch(err => {
                        errors.nosnippetfound = 'No snippets found';
                        res.status(404).json(errors);
                    });
            })
            .catch(err => {
                errors.nocategoryfound = 'No category with that id found';
                res.status(404).json(errors);
            });
    }
);

module.exports = router;
