const express = require('express');
const router = express.Router();
const mongoose = require('mongoose');
const passport = require('passport');
const shortid = require('shortid');

const validateSnippetInput = require('@validation/snippet');
const validateCommentInput = require('@validation/comment');

const Snippet = require('@models/Snippet');
const User = require('@models/User');
const Category = require('@models/Category');

function _populateUserAndCategory(snippet) {
    return new Promise((resolve, reject) => {
        let user = User.get({ id: snippet.userId });
        let category = Category.get({ id: snippet.categoryId });
        Promise.all([user, category]).then(vals => {
            delete vals[0].password;
            resolve({
                ...snippet,
                user: vals[0],
                category: vals[1]
            });
        });
    });
}

// @route   POST api/snippets
// @desc    Create snippet
// @access  Private
router.post(
    '/',
    passport.authenticate('jwt', { session: false }),
    (req, res) => {
        const { errors, valid } = validateSnippetInput(req.body);

        if (!valid) {
            return res.status(400).json(errors);
        }

        Category.queryOne('tag')
            .eq(req.body.tag)
            .exec()
            .then(category => {
                const newSnippet = new Snippet({
                    code: req.body.code,
                    description: req.body.description,
                    title: req.body.title,
                    userId: req.user.id,
                    categoryId: category.id
                });

                newSnippet
                    .save()
                    .then(snippet => res.json(snippet))
                    .catch(err => {
                        errors.internal = 'Internal server error';
                        res.status(404).json(errors);
                    });
            })
            .catch(err => {
                errors.nocategoryfound = 'That category doesn\t exist';
                res.status(404).json(errors);
            });
    }
);

// @route   GET api/snippets
// @desc    Get all snippets
// @access  Private
router.get(
    '/',
    passport.authenticate('jwt', { session: false }),
    (req, res) => {
        const errors = {};

        Snippet.getAll()
            .then(snippets => {
                let snippetsMapped = snippets.map(snippet => {
                    return _populateUserAndCategory(snippet);
                });

                Promise.all(snippetsMapped).then(result => {
                    if (result.length) {
                        res.json(result);
                    } else {
                        errors.nosnippetfound = 'No snippets found';
                        res.status(404).json(errors);
                    }
                });
            })
            .catch(err => {
                errors.nosnippetfound = 'No snippets found';
                res.status(404).json(errors);
            });
    }
);

// @route   GET api/snippets/:id
// @desc    Get snippet by id
// @access  Private
router.get(
    '/:id',
    passport.authenticate('jwt', { session: false }),
    (req, res) => {
        const errors = {};

        Snippet.get({ id: req.params.id })
            .then(snippet => {
                let snippetPopulated = _populateUserAndCategory(snippet);
                snippetPopulated
                    .then(result => {
                        res.json(result);
                    })
                    .catch(err => {
                        errors.internal = 'Internal server error';
                        res.status(404).json(errors);
                    });
            })
            .catch(err => {
                errors.nopostfound = 'No snippet with that ID found';
                res.status(404).json(errors);
            });
    }
);

// @route   DELETE api/snippets/:id
// @desc    Delete snippet by id
// @access  Private
router.delete(
    '/:id',
    passport.authenticate('jwt', { session: false }),
    (req, res) => {
        const errors = {};

        User.get({ id: req.user.id }).then(user => {
            Snippet.get({ id: req.params.id }).then(snippet => {
                if (snippet.userId.toString() !== req.user.id) {
                    errors.notauthorized = 'User not authorized';
                    return res.status(401).json(errors);
                }

                snippet
                    .delete()
                    .then(() => res.json({ success: true }))
                    .catch(err => {
                        errors.nosnippetfound = 'No snippet with that ID found';
                        res.status(404).json(errors);
                    });
            });
        });
    }
);

// @route   POST api/snippets/like/:id
// @desc    Like a snippet
// @access  Private
router.post(
    '/like/:id',
    passport.authenticate('jwt', { session: false }),
    (req, res) => {
        const errors = {};

        User.get({ id: req.user.id }).then(user => {
            Snippet.get({ id: req.params.id }).then(snippet => {
                console.log(snippet);
                // Check if the likes array exists
                if (!snippet.likes) {
                    snippet.likes = [];
                }

                if (
                    snippet.likes.filter(
                        like => like.userId.toString() === req.user.id
                    ).length > 0
                ) {
                    errors.alreadyliked = 'User already liked this snippet';
                    return res.status(400).json(errors);
                }

                snippet.likes.push({ userId: req.user.id });

                snippet
                    .save()
                    .then(snippet => res.json(snippet))
                    .catch(err => {
                        errors.internal = 'Internal server error';
                        res.status(404).json(errors);
                    });
            });
        });
    }
);

// @route   POST api/snippets/unlike/:id
// @desc    Un-like a snippet
// @access  Private
router.post(
    '/unlike/:id',
    passport.authenticate('jwt', { session: false }),
    (req, res) => {
        const errors = {};

        User.get({ id: req.user.id }).then(user => {
            Snippet.get({ id: req.params.id }).then(snippet => {
                if (
                    !snippet.likes ||
                    snippet.likes.length ||
                    snippet.likes.filter(
                        like => like.userId.toString() === req.user.id
                    ).length === 0
                ) {
                    errors.notliked = 'User has not liked the snippet';
                    return res.status(400).json(errors);
                }

                let removeIndex = snippet.likes
                    .map(item => item.userId.toString())
                    .indexOf(req.user.id);

                snippet.likes.splice(removeIndex, 1);

                snippet
                    .save()
                    .then(snippet => res.json(snippet))
                    .catch(err => {
                        errors.internal = 'Internal server error';
                        res.status(404).json(errors);
                    });
            });
        });
    }
);

// @route   POST api/snippets/comment/:id
// @desc    Comment on a snippet
// @access  Private
router.post(
    '/comment/:id',
    passport.authenticate('jwt', { session: false }),
    (req, res) => {
        const { errors, valid } = validateCommentInput(req.body);

        if (!valid) {
            return res.status(400).json(errors);
        }

        Snippet.get({ id: req.params.id })
            .then(snippet => {
                // Check if snippet has a comments array
                if (!snippet.comments) {
                    snippet.comments = [];
                }

                User.get({ id: req.user.id }).then(user => {
                    const newComment = {
                        id: shortid.generate(),
                        text: req.body.text,
                        name: user.name,
                        userId: user.id
                    };

                    snippet.comments.unshift(newComment);

                    snippet
                        .save()
                        .then(snippet => res.json(snippet))
                        .catch(err => {
                            console.log(err);
                            errors.internal = 'Internal server error';
                            res.status(400).json(errors);
                        });
                });
            })
            .catch(err => {
                errors.snippetnotfound = 'No snippet found';
                return res.status(404).json(errors);
            });
    }
);

// @route   DELETE api/snippets/comment/:id
// @desc    Delete a comment
// @access  Private
router.delete(
    '/comment/:id',
    passport.authenticate('jwt', { session: false }),
    (req, res) => {
        const errors = {};

        Snippet.get({ id: req.params.id })
            .then(snippet => {
                if (
                    !snippet.comments ||
                    !snippet.comments.length ||
                    snippet.comments.filter(
                        comment => comment.id.toString() === req.body.comment_id
                    ).length === 0
                ) {
                    errors.commentnotfound = 'Comment has not been found';
                    return res.status(404).json(errors);
                }

                let removeIndex = snippet.comments
                    .map(item => item.id.toString())
                    .indexOf(req.body.comment_id);

                snippet.comments.splice(removeIndex, 1);

                snippet
                    .save()
                    .then(snippet => res.json(snippet))
                    .catch(err => {
                        errors.internal = 'Internal server error';
                        res.status(404).json(errors);
                    });
            })
            .catch(err => {
                errors.snippetnotfound = 'No snippet found';
                return res.status(404).json(errors);
            });
    }
);

module.exports = router;
