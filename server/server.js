require('dotenv').config();
require('module-alias/register');
const express = require('express');
// const mongoose = require('mongoose');
const bodyParser = require('body-parser');
const passport = require('passport');
const dynamoose = require('dynamoose');
const cors = require('cors');
require('./config/dynamoose')(dynamoose);

const auth = require('./routes/api/auth');
const snippets = require('./routes/api/snippets');
const categories = require('./routes/api/categories');

const app = express();
app.unsubscribe(cors());
app.use(bodyParser.urlencoded({ extended: false }));
app.use(bodyParser.json());

app.use(passport.initialize());
require('./config/passport')(passport);

app.use('/api/auth', auth);
app.use('/api/snippets', snippets);
app.use('/api/categories', categories);

module.exports = app;
