const Validator = require('validator');
const isEmpty = require('./is-empty');

module.exports = function validateSnippetInput(data) {
    let errors = {};

    data.code = !isEmpty(data.code) ? data.code : '';
    data.title = !isEmpty(data.title) ? data.title : '';

    if (Validator.isEmpty(data.code)) {
        errors.code = 'Code field is required';
    }

    if (!Validator.isLength(data.title, { min: 10, max: 100 })) {
        errors.title =
            'Title for snippet must be between 10 and 100 characters';
    }

    if (Validator.isEmpty(data.title)) {
        errors.title = 'Title field is required';
    }

    return {
        errors,
        valid: isEmpty(errors)
    };
};
