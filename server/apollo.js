const { ApolloServer, gql } = require('apollo-server-express');
const passport = require('passport');
const schema = require('./apollo/schema');

const app = require('./server');
const port = process.env.PORT || 5000;

const server = new ApolloServer({
    schema,
    context: ({ req }) => ({
        baseUrl: `http://localhost:${port}`,
        auth: req.headers.authorization || false
    })
});

server.applyMiddleware({ app });

app.listen(port, () => {
    console.log(`Running at localhost:${port}. GraphQl: ${server.graphqlPath}`);
});
