import React, { Component } from 'react';
import { BrowserRouter as Router, Route } from 'react-router-dom';
import jwt_decode from 'jwt-decode';
import setAuthToken from './utils/setAuthToken';
import { setCurrentUser, logoutUser } from '@store/actions/auth';
import { Provider } from 'react-redux';
import store from '@store';

import Header from './components/layout/Header';
import Footer from './components/layout/Footer';

import Login from './components/auth/Login';
import Register from './components/auth/Register';
import Directory from './components/app/Directory';

import './App.scss';

if (localStorage.getItem('jwtToken')) {
    let token = localStorage.getItem('jwtToken');
    setAuthToken(token);
    const decoded = jwt_decode(token);
    store.dispatch(setCurrentUser(decoded));

    const currentTime = Date.now() / 1000;
    if (decoded.exp < currentTime) {
        store.dispatch(logoutUser());
        window.location.href = '/';
    }
}

class App extends Component {
    render() {
        return (
            <Provider store={store}>
                <Router>
                    <main className="wrapper">
                        <Header />
                        <Route exact path="/" component={Login} />
                        <Route exact path="/register" component={Register} />
                        <Route exact path="/directory" component={Directory} />
                        <Footer />
                    </main>
                </Router>
            </Provider>
        );
    }
}

export default App;
