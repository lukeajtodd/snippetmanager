import React from 'react';
import classnames from 'classnames';

export default props => {
    return (
        <div
            className={classnames(
                `${props.masonry ? 'masonry-container' : 'container'}`,
                props.extraClass
            )}
        >
            {props.children}
        </div>
    );
};
