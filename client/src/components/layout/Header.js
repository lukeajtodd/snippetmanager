import React from 'react';
import { FontAwesomeIcon } from '@fortawesome/react-fontawesome';
import { faFileAlt } from '@fortawesome/free-regular-svg-icons';

export default () => {
    return (
        <header className="header">
            <div className="header__logo">
                <FontAwesomeIcon icon={faFileAlt} color="white" size="3x" />
            </div>
        </header>
    );
};
