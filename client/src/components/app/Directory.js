import React, { Component } from 'react';
import PropTypes from 'prop-types';
import Container from '../layout/Container';
import Snippet from './Snippet';
import { connect } from 'react-redux';
import { getSnippets } from '@store/actions/app';

class Directory extends Component {
    constructor() {
        super();
        this.state = {
            errors: {}
        };
    }

    componentDidMount() {
        if (!this.props.auth.isAuthenticated) {
            this.props.history.push('/');
        }

        this.props.getSnippets();
    }

    componentWillReceiveProps(nextProps) {
        if (!nextProps.auth.isAuthenticated) {
            this.props.history.push('/');
        }

        if (nextProps.errors) {
            this.setState({
                errors: nextProps.errors
            });
        }
    }

    render() {
        const { errors } = this.state;

        return (
            <Container masonry={true} extraClass={`mt-4`}>
                {this.props.app.snippets &&
                    this.props.app.snippets.map(snippet => (
                        <Snippet key={snippet.id} snippet={snippet} />
                    ))}
            </Container>
        );
    }
}

Directory.propTypes = {
    getSnippets: PropTypes.func.isRequired,
    auth: PropTypes.object.isRequired,
    errors: PropTypes.object.isRequired
};

const mapStateToProps = state => ({
    app: state.app,
    auth: state.auth,
    errors: state.errors
});

export default connect(
    mapStateToProps,
    { getSnippets }
)(Directory);
