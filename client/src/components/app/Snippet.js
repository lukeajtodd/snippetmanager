import React from 'react';
import { Link } from 'react-router-dom';
import { FontAwesomeIcon } from '@fortawesome/react-fontawesome';
import * as faBrands from '@fortawesome/free-brands-svg-icons';
import {
    faCommentAlt,
    faStar,
    faArrowRight
} from '@fortawesome/free-solid-svg-icons';

export default ({ snippet }) => {
    let { likes, comments } = snippet;
    let { icon } = snippet.category;
    return (
        <div className="snippet">
            <div className="snippet__content">
                <h2 className="snippet__title">{snippet.title}</h2>
                <p className="snippet__description">{snippet.description}</p>
                <em className="snippet__postedby">
                    Created by {snippet.user.name}
                </em>
                {likes || comments ? (
                    <div className="snippet__info">
                        {comments ? (
                            <span className="snippet__comments">
                                {comments.length}
                                <FontAwesomeIcon icon={faCommentAlt} />
                            </span>
                        ) : null}
                        {likes ? (
                            <span className="snippet__likes">
                                {likes.length}
                                <FontAwesomeIcon icon={faStar} />
                            </span>
                        ) : null}
                    </div>
                ) : null}
                <Link to="/" className="btn btn--submit mt-1_4 d-inline-block">
                    View <FontAwesomeIcon icon={faArrowRight} color="#800fff" />
                </Link>
            </div>
            <FontAwesomeIcon
                icon={faBrands[`fa${icon[0].toUpperCase() + icon.slice(1)}`]}
                size="3x"
            />
        </div>
    );
};
