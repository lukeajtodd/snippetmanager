import React from 'react';
import classnames from 'classnames';
import PropTypes from 'prop-types';

const Input = ({
    name,
    placeholder,
    value,
    label,
    error,
    info,
    type,
    onChange,
    disabled
}) => {
    return (
        <div
            className={classnames('form__group', {
                'is-invalid': error
            })}
        >
            <input
                type={type}
                className="form__input"
                name={name}
                value={value}
                onChange={onChange}
                disabled={disabled}
            />
            <label
                className={classnames('form__label', {
                    'input-has-val': value !== ''
                })}
                htmlFor={name}
            >
                {placeholder}
            </label>
            {info && <small className="form__error">{info}</small>}
            <div className="invalid-feedback">{error ? error : null}</div>
        </div>
    );
};

Input.propTypes = {
    name: PropTypes.string.isRequired,
    placeholder: PropTypes.string,
    value: PropTypes.string.isRequired,
    info: PropTypes.string,
    error: PropTypes.string,
    type: PropTypes.string.isRequired,
    onChange: PropTypes.func.isRequired,
    disabled: PropTypes.string
};

Input.defaultProps = {
    type: 'text'
};

export default Input;
