import axios from 'axios';
import { GET_ERRORS, SET_SNIPPETS } from './types';

// Register
export const getSnippets = () => dispatch => {
    axios
        .get('api/snippets')
        .then(res => {
            dispatch({
                type: SET_SNIPPETS,
                payload: res.data
            });
        })
        .catch(err =>
            dispatch({
                type: GET_ERRORS,
                payload: err.response.data
            })
        );
};
