import { combineReducers } from 'redux';
import authReducer from './auth';
import errorReducer from './error';
import appReducer from './app';

export default combineReducers({
    app: appReducer,
    auth: authReducer,
    errors: errorReducer
});
