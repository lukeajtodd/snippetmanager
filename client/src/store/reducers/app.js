import { SET_SNIPPETS } from '@store/actions/types';

const initialState = {
    snippets: []
};

export default function(state = initialState, action) {
    switch (action.type) {
        case SET_SNIPPETS:
            return {
                ...state,
                snippets: action.payload
            };
        default:
            return state;
    }
}
