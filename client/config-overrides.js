const { override, addWebpackAlias } = require('customize-cra');
const path = require('path');

module.exports = override(
    addWebpackAlias({
        ['@store']: path.resolve(__dirname, 'src/store'),
        ['@utils']: path.resolve(__dirname, 'src/utils'),
        ['@validation']: path.resolve(__dirname, 'src/validation')
    })
);
