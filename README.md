# Snippet Manager (title to be decided)

_Still in development_

Currently using React and Express (GraphQL-ified) with AWS DynamoDB.

## Rundown:

-   Handle user authentication.
-   Snippets that can be created by a user.
-   Categories that a Snippet will correspond too.

## Getting Started (Server)

-   `npm i`
-   `npm run server`

## Getting Started (Client)

-   `yarn`
-   `yarn start`

### Pipeline

-   Make use of GraphQL (client).
-   Complete frontend build.
-   React -> Nerv
